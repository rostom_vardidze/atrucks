# Инициализация LoadBalancer сервера с помощью Ansible
Эта команда используется для инициализации сервера с помощью Ansible. Она запускает Ansible Playbook **init_server.yaml** и **letsencrypt_setup.yml** на удаленном сервере, указанным IP-адресом и портом.

## Требования
Перед запуском этой команды необходимо установить следующие программы:

**Ansible**

## Установка Ansible
Ansible можно установить с помощью пакетного менеджера вашей операционной системы:

На macOS с помощью Homebrew:

```bash
brew install ansible
```
На Ubuntu/ubuntu с помощью apt:

```bash
sudo apt update
sudo apt install ansible
```
На CentOS/RHEL с помощью dnf:

```bash
sudo dnf install ansible
```

## Использование
Чтобы запустить инициализацию ubuntu сервера укажите в файле inventory необходимый домен или IP-адрес сервера, в роли *ubuntu* заментите значения переменных на необходимые и выполните команду:
```bash
ansible-playbook init_server.yaml
```

Чтобы установить Let's Encrypt сертификат, замените в цикле *loop*, такси *Set up certificate* домен (можете указать сразу несколько доменов при необходимости) для которого хочите получить сертификат и выполните команду:
```bash
ansible-playbook letsencrypt_setup.yml
```
